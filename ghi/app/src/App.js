import { Fragment } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Nav from './Nav'
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <div>
      <BrowserRouter>
        <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/attendees">
            <Route index element={<AttendeesList attendees={props.attendees} />} />
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="/presentations/new" element={<PresentationForm />} />
        </Routes>
        </BrowserRouter>
    </div>
  );
}

export default App;
