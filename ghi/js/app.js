function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
        <div class="col-4">
        <div class="card shadow mx-1 mb-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-4 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
        <div class="card-footer">
            ${startDate} - ${endDate}
        </div>
        </div>
        </div>
        </div>
        `;
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.error("Can't fetch Conference List! Bad request!")

            const mainTag = document.querySelector('main');
            const errorHtml = `<div class="alert alert-warning" role="alert">
                                Can't fetch Conference List! Bad request!
                            </div>`
            mainTag.innerHTML = errorHtml


        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const location = details.conference.location.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts).toLocaleDateString();
                    // const startf = `${startDate.slice(5,7)}/${startDate.slice(8,10)}/${startDate.slice(0,4)}`;
                    const endDate= new Date(details.conference.ends).toLocaleDateString();
                    // const endf = `${endDate.slice(5,7)}/${endDate.slice(8,10)}/${endDate.slice(0,4)}`;


                    const html = createCard(name, description, pictureUrl, startDate, endDate, location);
                    const row = document.querySelector('.row');
                    row.innerHTML += html;
                } else {
                    console.error("Can't fetch Conference Detail! Bad Request!")
                    const mainTag = document.querySelector('main');
                    const errorHtml = `<div class="alert alert-warning" role="alert">
                    Can't fetch Conference Detail! Bad Request!
                  </div>`
                    mainTag.innerHTML = errorHtml
                }
            }
        }


    } catch (error) {
        console.error('Error')
        const mainTag = document.querySelector('main');
        const errorHtml = `<div class="alert alert-danger" role="alert">
                            Error!!!
                        </div>`
        mainTag.innerHTML = errorHtml

    }
});
